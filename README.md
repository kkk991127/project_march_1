# 학원 Lms 프로젝트
***
## 팀명 : March
### 
1. 안녕
2. _하세요_
3. ___저는___
4. **홍길동**입니다
***
* 동그라미
* 안녕안녕
***
* 여기 링크입니다 [바로가기](http://naver.com) 
참고사이트, 패키지 링크 , 걸어주기

![teacher1](./assets/images/teacher1.png)

1. 프로젝트 소개
2. 개발 기간
3. 개발자 소개 
4. 기술 스택 api java spring boot jpa ,
5. 프로젝트의 주요 기능 
6. 프로젝트 아키텍처
7. 릴리즈 노트 (피드백 받아온거 .. todo로 작성하고 선 찍 하면서 조치했다.)
## Installation
```
# Clone this repository
git clone https://github.com/WailanTirajoh/nuxt3-admin-template.git

# Go to directory
cd nuxt3-admin-template

# Run with npm
npm install
npm run dev

# Or with pnpm
pnpm install --shamefully-hoist
pnpm dev

# Install Husky
npx husky install
```


