import {acceptHMRUpdate, defineStore} from "pinia";

interface AdviceRequest {
  studentId: number
  studentName: string
  adviceTitle: string
  dateAdvice: string
  adviceContent: string
  etc: string

}

interface adviceDetail{
  studentId: number
  studentName : string
  adviceTitle: string
  dateAdvice: string
  adviceContent: string
  etc: string
}

export const useAdviceStore = defineStore('adviceManagement', {
  state: () => ({
    adviceData: [] as AdviceRequest[],
    adviceDetail:[] as adviceDetail[],
    currentPage: 1,
    pageSize: 10,
    totalItems: 0,
    totalPage: 0,
    totalCount:0,
  }),

  //http://35.193.177.229:8080/v1/adviceManagement/new -배포버전
  actions: {
    setAdvice(data: AdviceRequest) {
      const token = useCookie('token');
      console.log(token.value)
      // 배포 스웨거 주소
      $fetch('http://34.64.218.128:8080/v1/adviceManagement/new',{
        method:'POST',
        body : data,
        headers: {
          'Authorization': `Bearer ${token.value}`
        }
      });
      console.log('hello')
      console.log(data)

    },
    // 상담 리스트 가져오기
    async getAdviceList() {
      try {
        const { data }: any = await useFetch(
          `http://34.64.218.128:8080/v1/adviceManagement/all`,
          {
            method: 'GET',
          }
        );
        if (data) {
          this.adviceData = data.value.list;
          console.log(data.value.list);

        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    },

    // 상담 페이징 연동 하기
    async getPaging(pageNum:number, pageSize:number){
      const token = useCookie('token');
      const {data}: any = await useFetch(
        `http://34.64.218.128:8080/v1/adviceManagement/all/pageNum/${pageNum}`,
        {
          method: 'GET',
        });
      if (data) {
        this.adviceData = data.value.list;
      }
    },

    // 상담 상세 정보 가져오기 !
    async getDetail(detailId: string){
      const {id} = useRoute().params
      const {data}: any = await useFetch(
        `http://34.64.218.128:8080/v1/adviceManagement/detail/adviceManagementId/${id}`,
        {
          method: 'GET',
        });
      if (data) {
        this.adviceDetail = data.value;
      }
    }
  },



})


if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useAdviceStore, import.meta.hot))
}
