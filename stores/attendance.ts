import {acceptHMRUpdate, defineStore} from "pinia";

interface attendanceRequest{
  id : number
  studentName : string
  studentAttendance : number
  studentOut : number
  studentLate : number
  studentAbsence : number
  studentSick : number

}

export const useAttendanceStore = defineStore('attendance', {
  state: () => ({
    attendanceData: [] as attendanceRequest[],

  }),

  actions: {

    async getAttendanceList(){
      const {data}: any = await useFetch(
        `http://34.64.218.128:8080/v1/studentAttendance/all/`, {
          method: 'GET',
        });
      if (data) {
        this.attendanceData = data.value.list;
        console.log(data.value.list)
      }
    },
    //  페이징 연동  추후에 하기
    async getAttendancePaging(pageNum:number, pageSize:number){
      const token = useCookie('token');
      const {data}: any = await useFetch(
        `http://34.64.218.128:8080/v1/subjectStatus/all/pageNum/${pageNum}`,
        {
          method: 'GET',
        });
      if (data) {
        this.attendanceData = data.value.list;
      }
    },


  }

})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useAttendanceStore, import.meta.hot))
}
