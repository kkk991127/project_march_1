import {acceptHMRUpdate, defineStore} from "pinia";
interface teacherChangeRequest{
  imgSrc : string
  teacherName : string
  phoneNumber : string
  address : string
  dateBirth : string
  email : string
}

export const useTeacherStore = defineStore('teacher', {
  state: () => ({
    teacherChangeData: [] as teacherChangeRequest[],
  }),
  //http://35.193.177.229:8080/v1/adviceManagement/new -배포버전
  actions: {
    // 강사 수정하기
    async editProfile(data : teacherChangeRequest) {
      const token = useCookie('token');
      await useFetch(
        `http://34.64.218.128:8080/v1/teacher/changeInfo}`,
        {
          method: 'PUT',
          body: data,
          headers: {
            'Authorization': `Bearer ${token.value}`
          }

        }
      );

    },
    // 강사 토큰으로 상세정보 가져오기
    async getTeacherDetail() {
      const token = useCookie('token');
      console.log(token.value)
      try {
        const {data}: any = await useFetch(
          `http://34.64.218.128:8080/v1/teacher/detail`,
          {
            method: 'GET',
            headers: {
              'Authorization': `Bearer ${token.value}`
            }
          }
        );
        if (data) {
          this.teacherChangeData = data.value;
          console.log(data.value)
        }
      } catch (error) {
        console.error('토큰을 못찾아요. :', error);
      }
    },
  },
})


if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useTeacherStore, import.meta.hot))
}
