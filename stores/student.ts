// http://192.168.0.215:8080/v1/student/all
import {acceptHMRUpdate, defineStore} from "pinia";

// 훈련생 리스트 받아오기
interface studentRequest {
  studentName : string
  subjectId: string
  imgSrc : string
  gender : string
  dateBirth : string
  studentStatus : string
  statusWhy : string
  lateWho : string
}
interface studentDetail{
  id: number
  subjectChoice: string
  imgSrc: string
  studentName: string
  gender: string
  dateBirth: string
  phoneNumber: string
  idNumber: string
  address: string
  email: string
  finalEducation:string
  major: string
  studentStatus: string
  dateStatusChange: string
  statusWhyAndDate: string
  dateJoin: string
}



// 데이터의 타입을 지정해줘야함
// auth 스토어

export const useStudentStore = defineStore('tableData', {
  state: () => ({
    tableData: [],
    studentDetail: [] as studentDetail[]

  }),

  actions: {
    async getList() {
      const {data}: any = await useFetch(
        `http://34.64.218.128:8080/v1/student/all`, {
          method: 'GET',
        });
      if (data) {
        this.tableData = data.value.list;
        console.log(this.tableData, 3)

      }
    },
    // 수강생페이징
    async getPaging(pageNum:number, pageSize:number){
      const token = useCookie('token');
      const {data}: any = await useFetch(
        `http://34.64.218.128:8080/v1/student/all/pageNum/${pageNum}`,
        {
          method: 'GET',
        });
      if (data) {
        this.tableData = data.value.list;
      }
    },
    // todo : 새로고침 해야 나옴.
    async getDetailList(studentId: string) {
      const { data }: any = await useFetch(
        `http://34.64.218.128:8080/v1/student/detail/studentId-by-Teacher/${studentId}`,
        {
          method: 'GET',
        }
      );
      if (data) {
        this.studentDetail = data.value; // 서버 응답의 데이터를 tableDetail에 할당
      } else {
        console.error('서버 응답에 실패하였습니다.');
      }
    },
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useStudentStore, import.meta.hot))
}

